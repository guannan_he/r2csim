This program generates random data for the purpose of r2c dashboard
development.  It simulates the resource activity and events happening in a
data replication environment.  It records simulation data into a set of
circular buffers.  The buffers are arranged in a hierarchy based on time
increment.  When a lower buffer becomes full, its values are reduced (by
counting or averaging) into a single level higher bucket value.

                              
                ...                 days

    ----------------------           
   | | | | | | |...| | | |          hours
    0                   24         
    ----------------------
   | | | | | | |...| | | | | | | |  minutes
    -----------------------------
    0                          59

  Metric Structure: hierarchy of circular buffers

  
The values (referred to as metrics) in the buffers are timestamped.  The types
of the metric values and 'reduce' method may vary depending on what is being
measured.  For example, if we are measuring the current number of bytes on a
disk, we reduce the buffer to its mean value.  If we are collecting an on/off
(0/1) state, we reduce by summation. 

Each element in the service model maintains an array of metrics.  (The metrics
are currently incomplete - more will be added over time).

The service elements currently are:

 server
    disks[]                                           diskImage
                                             
 server                                               diskImage
    disks[]  --  sourceGW  -- network --  targetGW  
                                                        ...
    ...                                              
  server                                              diskImage
    disks[]
       
server:
  attributes:  name, state, applications[]
  metrics:     state

disk:
  attributes:  name, state, capacity, utilization
  metrics:     state, utilization, alarms

sourceGW:
  attributes:  name
  metrics:     buffer, rpo

network:
  attributes:  bandwidth
  metrics:     TBD


targetGW: TBD

diskImage: TBD


Files:

r2csim.config.js:  The r2csim module is configured via this file.  The data in
this file allows the user (developer) to choose the parameters of the
simulation (e.g. the number of servers, number and sizes of disks, disk write
rates etc).  Currently, the configuration is applied when the javascript
is loaded by the web browser, so the r2csim.js file must be loaded first.

r2csim.js: Main source code for the module.

index.html: Sandbox web page demonstrating required load order (d3.js, 
r2csim.js, r2csim.config.js).


Usage:

To generate data, configure as desired, load the index file, then in the web
console run:

// create a service instance
> s = r2csim.service();

// run the service (providing a number of seconds or default to 3 days):
> s.run();

// use the data for you development needs e.g. to see the utilization for
//  server[0], disk[0] do:

> s.servers[0].disks[0].metrics[1].name()
"util"
> s.servers[0].disks[0].metrics[1].data()
Object {60000: Array[60], 3600000: Array[24], 86400000: Array[3]}


To plot a guage type metric:

> util = s.servers[0].disks[0].metrics[1] 
> util.chart(r2csim.consts.DAY)

try to make a pull request
