(function () {
  var config = {};

  // values for simulating resource activity and consumption
  config.metrics = {
    cpuUser: { desc: 'CPU User', type: 'value', min: 0, max: 50, t: r2csim.consts.MIN, unit: 'pct' },
    cpuSys: { desc: 'CPU System', type: 'value', min: 0, max: 50, t: r2csim.consts.MIN, unit: 'pct' },
    bytesWritten: { desc: 'Bytes Written', type: 'rate', min: -100*r2csim.consts.K, mean: 300*r2csim.consts.K, 
      t: r2csim.consts.SEC, unit: 'bytes/sec' },
    diskAlerts: { desc: 'Disk Alert Rate', type: 'rate', min: 0, mean: 2, t: r2csim.consts.HR, unit: 'count/sec' }
  };

  // resource availability 
  config.avail = {
    pserver: { desc: 'Protected Server Availability', ttf: 30*r2csim.consts.DAY, ttr:r2csim.consts.HR },
    disk: { desc: 'Protected Disk Availability', ttf: 30*r2csim.consts.DAY, ttr:r2csim.consts.HR },
    agent: { desc: 'Agent Availability', ttf: 30*r2csim.consts.DAY, ttr: r2csim.consts.HR },
    server: { desc: 'Infra Server Availability',  ttf: r2csim.consts.YEAR, ttr: 4*r2csim.consts.HR },
    network: { desc: 'Infra Network Availability', ttf: r2csim.consts.YEAR, ttr: 4*r2csim.consts.HR },
    storage: { desc: 'Infra Storage Availability', ttf: r2csim.consts.YEAR, ttr: 4*r2csim.consts.HR }
  };

  // sizes and counts
  config.sizes = {
    servers: { desc: 'Protected Servers', min: 10, max: 10, unit: 'count' },
    disks: { desc: 'Protected Disks Per Server', min: 1, max: 1, unit: 'count' },
    apps: { desc: 'Protected Applications Per Server', min: 1, max: 3, unit: 'count' },
    diskCapacity: { desc: 'Protected Disk Capacity', min: 500*r2csim.consts.G, max: 500*r2csim.consts.G, unit: 'bytes' },
    diskUtil: { desc: 'Protected Disk Initial Utilization', min: 10, max: 50, unit: 'pct' },
    replicationBW: { desc: 'Replication Bandwidth', min: 320*r2csim.consts.Mbps, max: 320*r2csim.consts.Mbps, unit: 'bps' },
    storageCapacity: { desc: 'Infrastructure Storage Capacity', min: 1000*r2csim.consts.G, max: 1000*r2csim.consts.G, unit: 'bytes' },
    storageUtil: { desc: 'Infrastructure Storage Initial Utilization', min: 10, max: 30, unit: 'pct' }
  };

  // high and low watermarks for generating alerts
  config.limits = {
    iStorage: { desc: 'Infrastructure Storage', low: 75, high: 80, unit: 'pct' },
    iBandwidth: { desc: 'Infrastructure Bandwidth', low: 75, high: 80, unit: 'pct' }
  };

  // simulation related
  config.sim = {
    startDate: new Date("10 November 1967"),
    tickSize: r2csim.consts.MIN
  };

  r2csim.config(config);
}());
