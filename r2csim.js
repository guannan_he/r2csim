/**
* An r2c simulator/ data model.  These functions simulate the activity of a set
* of protected servers and replication infrastructure over a time interval.
* The purpose of this script is to produce data that approxomate the data of
* the actual replication service.  The data can be used as input to dashboard
* UI development.
* 
* Requires d3.js: http://d3js.org/d3.v3.min.js
*/
r2csim = (function() {
  var config, r2csim = {};

  r2csim.config = function(config_) {
    if (arguments.length < 1) {
      return config;
    }
    config = config_;
  };

  var consts = r2csim.consts = (function() {
    var MILLIS = 1, SEC = 1000*MILLIS, MIN = 60*SEC, HR = 60*MIN, DAY = 24*HR,
      YEAR = 365*DAY;  // ignore details like leap year

    return {
      MILLIS: MILLIS, SEC: SEC, MIN: MIN, HR: HR, DAY: DAY, YEAR: YEAR,
      SEC_MIN: 60, MIN_HR: 60, HR_DAY: 24, DAY_YEAR: 365,
      K: 1e3, M: 1e6, G: 1e9, Mbps: 1e6,
      UC_LETTERS: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
      LC_LETTERS: 'abcdefghijklmnopqrstuvwxyz',
      NUMBERS: '0123456789'
    };
  }());

  /**
   * Generate a random integer.
   *
   * @param min min value
   * @param max max value
   * @return a random int in range min, max inclusive
   */ 
  function rint(min, max) {
    if (min > max) { return undefined; }
    return min + (Math.floor(Math.random() * 1e16) % (max + 1 - min));
  }

  /**
   * Return a random string of len letters from alphabet.
   */
  function rstring(alphabet, len) {
    var ret = '';
    for (var i = 0; i < len; ++i) {
      ret += alphabet.charAt(rint(0, alphabet.length-1));
    }
    return ret;
  }

  /**
   * Return a random string of len letters from lower case characters.
   */
  function ralphastring(len) {
    return rstring(consts.LC_LETTERS, len);
  }

 /**
   * Return a random string of len letters from alphanumeric characters.
   */
  function ralphanumstring(len) {
    return rstring(consts.UC_LETTERS + consts.LC_LETTERS + 
             consts.NUMBERS, len);
  }

  /**
   * Pick n values from an array items.
   *
   * @param n number of values to select
   * @return n randomly chosen values from items array
   */
  function rselection(items, n) {
    var swap = function (items, p1, p2) {
      var tmp = items[p1];
      items[p1] = items[p2];
      items[p2] = tmp;
    };
    if (n === 0) {
      return [];
    } else if (n < 0) {
      throw('sample negative');
    } else if (n > items.length) {
      throw('sample larger than population');
    }
    var last = items.length - 1;
    for(var i = 0; i < n; ++i) {
      swap(items, rint(0, last - i), last - i);
    }
    return items.slice(items.length - n, items.length);
  }

  /**
   * @return a random disk name
   */
  function rdiskName(base) {
    return (arguments.length < 1 ? 'Disk ' : base) + ralphastring(3).toUpperCase();
  }

  /**
   * @return a random alert string.
   */
  function ralertNames (n) {
    var names = ['Error 1', 'Error 2', 'Error 3', 'Warning 1', 'Warning 2', 
      'Warning 3']; 
    return rselection(names, n);
  }

  /**
   * @return a random application name
   */
  function rapplicationNames (n) {
    var names = [
      'Utilities', 'Safari', 'App Store', 'Calculator ', 'Chess', 'Dictionary',
      'Launchpad', 'Mission Control', 'Time Machine', 'FaceTime', 'QuickTime Player', 
      'Font Book', 'Image Capture', 'Photo Booth', 'Preview',
      'Stickies', 'System Preferences', 'TextEdit', 'Dashboard', 'Mail',
      'Address Book', 'iCal', 'Automator', 'DVD Player', 'iTunes', 'iChat',
      'Microsoft Communicator', 'Microsoft Messenger', 'Microsoft Office 2011'];
    return rselection(names, n);
  }

  /**
   * @return a random server name
   */
  function rserverNames (n) {
    var names =  ['Struthioniformes', 'Anseriformes', 'Galliformes',
      'Charadriiformes', 'Gruiformes', 'Podicipediformes', 'Ciconiiformes',
      'Pelecaniformes', 'Procellariiformes', 'Sphenisciformes',
      'Columbiformes', 'Psittaciformes', 'Cuculiformes', 'Falconiformes',
      'Strigiformes', 'Caprimulgiformes', 'Apodiformes', 'Coraciiformes',
      'Piciformes', 'Passeriformes'];
    return rselection(names, n);
  }

  /**
   * A circular buffer.
   */
  function cbuffer(size) {
    var cbuffer = {}, len = 0, buf = Array(size);

    /**
     * Return the last element pushed info the buffer.
     */
    cbuffer.last = function() {
      return len > size ? buf[len % size === 0 ? size - 1 : (len % size) - 1] : 
        buf[len - 1];
    };

    /**
     * Return the number of elements in the buffer.
     */
    cbuffer.length = function() {
      return Math.min(len, size);
    };

    /**
     * Check if buffer is full.
     */
    cbuffer.full = function() {
      return this.length() === size;
    };

    /**
     * @return the arithmetic mean of buffer values
     */
    cbuffer.mean = function() {
      return Math.round(d3.mean(buf));
    };

    /**
     * @return the arithmetic mean of buffer values
     */
    cbuffer.sum = function() {
      return d3.sum(buf);
    };

    /**
     * Check if buffer is empty.
     */
    cbuffer.empty = function() {
      return this.length() === 0;
    };

    /**
     * Push a value, returning the displaced value or zero if
     *   position hasn't been written to.
     */
    cbuffer.push = function(value) {
      var ret = buf[len % size];
      buf[len % size] = value; 
      len += 1;
      return ret === undefined ? 0 : ret;
    };

    /**
     * @return a copy of values in normal array (unwrapped) order.
     */
    cbuffer.toArray = function() {
      return len > size ? buf.slice(len % size).concat(buf.slice(0, len % size)) :
        buf.slice(0, len);
    };

    /**
     * call reduce on underlying array.
     */
    cbuffer.reduce = function(callback, initialValue) {
      return buf.reduce(callback, initialValue);
    };

    return cbuffer;
  }

  /**
   * A time series buffer.  Holds (key, value) pairs where key is timestamp and
   * value is a value to track over time.  Buffers are circular and nested.
   * When a child buffer is full (or a full cycle completes), its values are
   * reduced to a single entry in the parent buffer.
   */  
  function tsbase(size, interval, child) {
    var tsbase = {}, times = cbuffer(size), values = cbuffer(size);

    tsbase.interval = interval;
    tsbase.child = child;

    tsbase.full = function() { return times.full(); };
    tsbase.empty = function() { return times.empty(); };

    /**
     * Add a time, value pair, reducing as required.
     */
    tsbase.push = function(time, value) {
      var child_values;
      if (child) {
        child_values = child.push(time, value);
        if (child.full() && (this.empty() || 
              (time - times.last()) >= interval)) {
          times.push(time);
          values.push(child.reduce(child_values));
        }
      } else {
        times.push(time);
        values.push(value);
      }
      return values;
    };

    /**
     * @return the zipped times and values.
     */
    tsbase.data = function() {
      return d3.zip(times.toArray(), values.toArray());
    };

    /**
     * @return the zipped times and values recursively.
     */
    tsbase.rdata = function()  {
      var ret = {}, next = this;
      while (next) {
        ret[next.interval] = next.data();
        next = next.child;
      }
      return ret;
    };

    return tsbase;
  }

  /**
   * Return the mean of an array. If the array values are d3.map return a
   * d3.map of mean values across common keys.
   */
  function mean(values) {
    if (values.length === 0)
      return undefined;

    if (values.last().keys === undefined) {
      return values.mean();
    } else {
      var sum = values.reduce(function (prev, curr, i, array) {
        curr.forEach(function (key, value) {
          prev.set(key, (prev.get(key) === undefined ? 0 : prev.get(key)) + 
             value);
        });
        return prev;
      }, d3.map());
      sum.forEach(function (key, value) {
        sum.set(key, Math.round(value/sum.values().length));
      });
      return sum;
    }
  }

  /**
   * Return the sum of an array. If the array values are d3.map return a d3.map
   * of sum values across common keys.
   */
  function sum(values) {
    if (values.length === 0)
      return undefined;

    if (values.last().keys === undefined) {
      return values.sum();
    } else {
      var sum = values.reduce(function (prev, curr, i, array) {
        curr.forEach(function (key, value) {
          prev.set(key, (prev.get(key) === undefined ? 0 : prev.get(key)) + 
             value);
        });
        return prev;
      }, d3.map());
      return sum;
    }
  }

  /**
   * A time series buffer where the mean of child values becomes an entry in
   * the parent buffer.
   */
  function tsbuffer(size, interval, child) {
    var buffer = tsbase(size, interval, child);

    buffer.reduce = function(values) {
      return mean(values);
    };

    return buffer;
  }

  /**
   * A time series buffer where the sum of child values becomes an entry in
   * the parent buffer.
   */
  function tscollector(size, interval, child) {
    var buffer = tsbase(size, interval, child);

    buffer.reduce = function(values) {
      return sum(values);
    };

    return buffer;
  }
 
  /**
   * A base class for metrics. Increments time and records current value.
   */
  function metric(value, buffer) {
    var metric = {}, target, name, currentValue, 
         currentTime = config.sim.startDate.getTime();   

    /**
     * get/set target 
     */
    metric.target = function(tvalue) {
      if (arguments.length === 0)
        return target;

      target = tvalue;
      return this;
    };

    /**
     * get/set name 
     */
    metric.name = function(nvalue) {
      if (arguments.length === 0)
        return name;

      name = nvalue;
      return this;
    };

    /**
     * return the last measure time, value
     */
    metric.last = function() {
      return [currentTime, currentValue];
    };

    /**
     * Increase time by interval 
     */
    metric.tick = function(interval) {
      currentTime += interval;
      currentValue = value.call(this, interval, this.last());
      buffer.push(currentTime, currentValue);
      return this.last();
    };

    metric.data = function() {
      return buffer.rdata();
    };

    return metric;
  }

  /**
   * A metric that is represented by a toggle on/off value.
   */
  function toggle(value) {
    return metric(value, tsbuffer(consts.DAY_YEAR, consts.DAY, 
                 tsbuffer(consts.HR_DAY, consts.HR, 
                 tsbuffer(consts.MIN_HR, consts.MIN))));
  }

  /**
   * A metric that is represented by a continous scalar value at a given
   * interval.
   *
   * @param value - a function that generates a value at a time interval
   */
  function gauge(value) {
    var m = metric(value, tsbuffer(consts.DAY_YEAR, consts.DAY, 
                 tsbuffer(consts.HR_DAY, consts.HR, 
                 tsbuffer(consts.MIN_HR, consts.MIN))));

    m.chart = function(interval, relement) {
      if (arguments.length < 2)
        relement = 'chart';
      if (arguments.length < 1)
        interval = consts.MIN;
      new Highcharts.Chart(this.hcconfig(interval, relement));
    };

    m.hcconfig = function (interval, relement) {
      if (arguments.length < 2)
        relement = 'chart';
      if (arguments.length < 1)
        interval = consts.MIN;

      var data = this.data()[interval];

      return {
        chart: {
          type: 'line',
          zoomType: 'x',
          spacingRight: 20,
          renderTo: relement
        },

        title: { 
          text: this.name()
        },

        xAxis: {
          type: 'datetime',
        },

        yAxis: {
          title: { 
            text: 'TBD' 
          }
        },

        series: [{
          data: data
        }]
      };
    };

    return m;
  }


  /**
   * A metric that is represented by an object occurence count at a given
   * interval.
   *
   * @param value - a function that generates a value at a time interval
   */
  function counter(value) {
    return metric(value, tscollector(consts.DAY_YEAR, consts.DAY, 
                 tscollector(consts.HR_DAY, consts.HR, 
                 tscollector(consts.MIN_HR, consts.MIN))));
  }

  /**
   * Create a value function that returns a dictionary of metric values keyed
   * by metric name.
   *
   * @param sources - source metrics
   */
  function dict(sources) {
    return function(interval) {
      var name, ret = d3.map();
      sources.forEach(function (metric) {
        name = "[" + metric.target().name + "][" + metric.name() + "]";
        ret.set(name, metric.last()[1]);
      });
      return ret;
    };
  }

  /**
   * A value function that returns the difference between the current and last values
   * of keys in a dictionary.
   *
   * @param value a function that returns the current value
   */
  function diff (value) {
    var lastValue;
    return function(interval) {
      var ret = d3.map(), currentValue = value(interval);
      currentValue.forEach(function(key, value) {
        if (lastValue === undefined) {
          ret.set(key, 0);
        } else {
          ret.set(key, value - lastValue.get(key));
        }
      });
      lastValue = currentValue;
      return ret;
    };
  }

  /**
   * Return function that increases at an average rate of count/period. e.g. to
   * increase on average
   *
   *    diskUtilization = increasing([-100*k, 640*K], SEC);
   *
   * @param range [min, mean] count
   * @param period the minimum period in which to produce a non-zero value
   */
  function increasing(range, period) {
    var cumm = 0;

    return function(interval) {
      var ret = 0;
      cumm += interval;
      if (cumm >= period) {
        var n = Math.floor(cumm/period);
        ret += n * rint.apply(null, range);
        cumm -= n*period;
      }
      return ret;
    };
  }

  /**
   * @return a function that evaluates to true once per period
   */
  function event(period) {
    var currentTime = config.sim.startDate.getTime();
    var next = currentTime + rint(0, period);

    return function(interval) {
      currentTime += interval;
      if (currentTime >= next) {
        while (next <= currentTime) 
          next += rint(0, period);
        return true;
      }
      return false;
    };
  }

  /**
   * Filter list by name.
   */
  function metricByName(name) {
    return this.metrics.filter(function(metric) {
        return metric.name() === name;
    })[0];
  }

  /**
   */
  function range(config) {
    if (config.min && config.max) 
      return [config.min, config.max];
    if (config.min && config.mean) 
      return [config.min , 2*config.mean - config.min];
  }

  /**
   * Add metrics to the disk data structure with metrics and methods to
   * simulate their values over time.
   *
   * @param disk the disk to instrument
   */
  function instrumentDisk(disk) {
    var bytesWritten = increasing(range(config.metrics.bytesWritten),
      config.metrics.bytesWritten.t);
    var alerts = increasing(range(config.metrics.diskAlerts), 
      config.metrics.diskAlerts.t);
    var diskRecovered = event(config.avail.disk.ttr);
    var diskFailed = event(config.avail.disk.ttf);

    disk.metrics = [];

    disk.metrics.push(gauge(function (interval, prev) {
          var disk = this.target();
          var failed = diskFailed(interval);
          var recovered = diskRecovered(interval);
          if (disk.state === 0 && failed) {
            disk.state = 1;
          } else if (disk.state === 1 && recovered)  {
            disk.state = 0;
          }
          return disk.state;
      }).name('state').target(disk));

    disk.metrics.push(gauge(function (interval, prev) {
          var disk = this.target();
          var b = bytesWritten(interval);
          if (disk.server.state === 0 && 
                     disk.state === 0) {
            return disk.util += b;   
          } else {
            return disk.util;
          }
      }).name('util').target(disk));

    disk.metrics.push(counter(function (interval, prev) {
          var disk = this.target();
          var ret = d3.map();
          ralertNames(alerts(interval)).forEach(function(value) { 
            if (disk.server.state === 0 && disk.state === 0)
              ret.set(value, 1);
          }, disk);
          return ret;
      }).name('alert').target(disk));

    disk.tick = function (interval) {
      return this.metrics.map(function(metric) {
        return [metric, metric.tick(interval)];
      });
    };

    return disk;
  }

  /**
   * Add metrics to the server data structure with metrics and methods to
   * simulate their values over time.
   *
   * @param server the server to instrument
   */
  function instrumentServer (server) {
    var serverRecovered = event(config.avail.server.ttr);
    var serverFailed = event(config.avail.server.ttf);

    server.metrics = [];

    server.metrics.push(
      gauge(function (interval, prev) {
          var server = this.target();
          var failed = serverFailed(interval);
          var recovered = serverRecovered(interval);
          if (server.state === 0 && failed) {
            server.state = 1;
          } else if (server.state === 1 && recovered)  {
            server.state = 0;
          }
          return server.state;
      }).name('state').target(server));

    server.tick = function (interval) {
      var ret;
      ret = this.metrics.map(function(metric) {
        return [metric, metric.tick(interval)];
      });
      this.disks.forEach(function(disk) {
        disk.tick(interval);
      });
      return ret;
    };

    return server;
  }

  /**
   * Add metrics to the source gw data structure with metrics and methods to
   * simulate their values over time.
   *
   * @param gw the server to instrument
   */
  function instrumentSourceGateway (gw) {
    var buffer = (function() {
      var curr, 
          buffer = {}, 
          lastTx = d3.map(), 
          rpo = d3.map(),  
          bwBps = gw.network.effBandwidth/8;

      /**
       * Add the values of matching keys and assign to target.
       */
      function add(source, target) {
        source.forEach(function(key, value) {
          target.set(key, target.get(key) + value);
        });
      }

      /**
       * Drain the contents of the buffer to model network
       */
      function drain(time, interval) {
        var available = (interval/1000) * bwBps,
           i = 0, key, value, keys = d3.shuffle(curr.keys().slice(0));

        while (available > 0 && i < keys.length) {
          key = keys[i++];
          value = curr.get(key);
          if (value > 0) {
            if (available >= value) {
              available -= value;
              value = 0;
              // the last time that bytes were sent to remote
              lastTx.set(key, time);
            } else {
              value -= available;
              available = 0;
            }
            curr.set(key, value);
          }
        }

        // calculate the rpo for each disk
        curr.forEach(function (key, value) {
          if (value <= 0) {
            rpo.set(key, 0);   // zero rpo when empty buffer
          } else if (lastTx.has(key)) {
            rpo.set(key, time - lastTx.get(key));
          }
        });
      }

      /**
       * Calculate the contents of the buffer.
       */
      buffer.calc = function(interval, next, prev) {
        if (curr === undefined) {
          curr = d3.map(next);
        } else {
          add(next, curr); 
        }
        drain(prev[0] + interval, interval);
        return next;
      };

      /**
       * Return a copy of the current buffer.
       */
      buffer.current = function() {
        return d3.map(curr);
      };

      /**
       * Return a copy of current rpo.
       */
      buffer.rpo = function() {
        return d3.map(rpo);
      };

      return buffer;
    }());

    var diskUtils = [];

    // collect [] of diskUtils
    gw.servers.forEach(function(server) {
      return server.disks.forEach(function(disk) {
        diskUtils.push(metricByName.call(disk, 'util'));
      });
    });

    var bytesWritten = diff(dict(diskUtils));

    gw.metrics = [];

    gw.metrics.push(counter(function (interval, prev) {
        var b = bytesWritten();
        buffer.calc(interval, b, prev);
        return b;
      }).name('bytesWritten').target(gw));
  
    gw.metrics.push(  counter(function (interval, prev) {
        return buffer.current();
      }).name('buffer').target(gw));
  
    gw.metrics.push(  counter(function (interval, prev) {
        return buffer.rpo();
      }).name('rpo').target(gw));

    gw.tick = function (interval) {
      return this.metrics.map(function(metric) {
        return [metric, metric.tick(interval)];
      });
    };

    return gw;
  }

  /**
   * Disk data with random capacity and initial utilization.
   *
   * @param n the number of disks to create
   */
  r2csim.rdisks  = function(server, n) {
    var rutil = range(config.sizes.diskUtil);
    var rcapacity = range(config.sizes.diskCapacity);

    var disks = [], capacity, util;

    while (n--) {
      capacity = rint.apply(null, rcapacity);
      util =  Math.floor(rint.apply(null, rutil) * capacity / 100); 

      disks.push(instrumentDisk({
        server: server,
        name: "[" + server.name + "[" + rdiskName() + "]]",
        capacity: capacity, 
        util: util,
        state: 0
      }));
    }
    return disks;
  };

  /**
   * Server data with up/down state and a random number of disks.
   *
   * @param n [min, max] number of servers
   */
  r2csim.rservers = function(n) {
    var rndisks = range(config.sizes.disks);
    var rnapps = range(config.sizes.apps);

    var server, servers = [];

    while (n--) {
      server = {
        name: rserverNames(1)[0],
        applications: rapplicationNames(rint.apply(null, rnapps)),
        state: 0
      };
      server.disks = r2csim.rdisks(server, rint.apply(null, rndisks))

      servers.push(instrumentServer(server));
    }
    return servers;
  };

  /**
   * Network. This element represents available WAN bandwidth.
   *
   */
  r2csim.network = function() {
    return {
      name: "WAN Network",
      effBandwidth: rint(config.sizes.replicationBW.min,
       config.sizes.replicationBW.max), 
    };
  };

  /**
   * Replication data source gateway. This element aggregates and compresses
   * server data across network to target gateway.
   *
   * @param servers
   */
  r2csim.sourceGateway = function(servers, network) {
    return instrumentSourceGateway({
      name: "Source Replication Gateway",
      servers: servers,
      network: network,
      capacity: config.sizes.storageCapacity.min,
      util: Math.floor(config.sizes.storageUtil.min *
             config.sizes.storageCapacity.min / 100),
      state: 0   
    });
  };

  /**
   * A disk image represents the remote data sink for a disk.  Its size is
   * proportional to the disk utilization (subject to the replication latency).
   */
  r2csim.diskImage = function(disk) {};

  /**
   * The target gateway.  The target gateway demultiplexes the replication
   * stream and stores bytes
   *
   * @param network
   */
  r2csim.targetGateway = function(network, images) {};

  /**
   * Builds and returns an object representing the service as a whole.
   */
  r2csim.service = function () {
    var service = {};
    
    service.servers = r2csim.rservers(rint.apply(null, 
      range(config.sizes.servers)));

    service.network = r2csim.network();

    service.sourceGW = r2csim.sourceGateway(service.servers, 
      service.network);

    /**
     * Create test data by calling tick on top level elements.
     */
    service.run = function(duration) {
      if (arguments.length < 1) 
        duration = 3 * consts.DAY;

      for (var i = 0; i < duration/config.sim.tickSize; ++i) {
        this.servers.forEach(function(server) {
          server.tick(config.sim.tickSize);
        });
        this.sourceGW.tick(config.sim.tickSize);
      }
    };

    service.render = function() {
      d3.select("body").selectAll("div")
        .data(this.servers).enter().call(serverIcon, 100, 100);
    };

    return service;
  };
   
  // renders a sample SVG icon from Amazon AWS
  //   http://aws.amazon.com/architecture/icons
  function serverIcon(selection, w, h) {
    //var f1 = "#D9A741", f2 = "#876929";
    var f1 = "red", f2 = "blue", f3 = "green", f4 = "yellow";

    var g = selection.append("svg")
       .attr("width", w + "px")
      .attr("height", h + "px").append("g");

    g.append("rect")
      .attr("x", "31.119")
      .attr("y", "28.683")
      .attr("fill", f1)
      .attr("width", "10.008")
      .attr("height", "39.254");

    g.append("rect")
      .attr("x", "31.119")
      .attr("y", "67.937")
      .attr("fill", f2)
      .attr("width", "10.008")
      .attr("height", "3.381");

    g.append("path")
      .attr("fill", f3)
      .attr("d", "M44.988,28.683h10.006v39.254H44.988V28.683z M52.887,65.845V30.771h-5.793v35.074H52.887");

    g.append("rect")
      .attr("x", "44.988")
      .attr("y", "67.937")
      .attr("fill", f2)
      .attr("width", "10.006")
      .attr("height", "3.381");

    g.append("path")
      .attr("fill", f4)
      .attr("d", "M58.855,28.683h10.023v39.254H58.855V28.683z M66.773,65.845V30.771h-5.812v35.074H66.773");

    g.append("rect")
      .attr("x", "58.855")
      .attr("y", "67.937")
      .attr("fill", f2)
      .attr("width", "10.023")
      .attr("height", "3.381");

    return selection;
  };

  return r2csim;
}());
